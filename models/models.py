from pydantic import BaseModel


class UserModel(BaseModel):
    username: str
    email: str

    class Config:
        orm_mode = True


class UserInput(UserModel):
    password: str


class BlogModel(BaseModel):
    title: str
    body: str
    user_id: int

    class Config:
        orm_mode = True


class ShowBlogModel(BlogModel):
    pass


class UserOutModel(UserModel):
    id: int
    # blogs: list[BlogOutModel] = []


class BlogOutModel(BlogModel):
    id: int
    creator: UserOutModel


class Login(BaseModel):
    email: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: str | None = None
