from datetime import datetime, timedelta
from os import getenv

from jose import JWTError, jwt

import models


def create_access_token(data: dict):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=int(getenv('ACCESS_TOKEN_EXPIRE_MINUTES')))
    to_encode.update({'exp': expire})
    encoded_jwt = jwt.encode(to_encode, getenv('SECRET_KEY'), algorithm=getenv('ALGORITHM'))
    return encoded_jwt


def verify_token(token: str, credentials_exception):
    try:
        payload = jwt.decode(token, getenv('SECRET_KEY'), algorithms=[getenv('ALGORITHM')])
        email: str = payload.get('sub')

        if email is None:
            raise credentials_exception

        token_data = models.TokenData(email=email)
    except JWTError:
        raise credentials_exception

    return token_data
