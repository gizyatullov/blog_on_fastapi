from passlib.context import CryptContext


class Hash:
    def __init__(self):
        self.pwd_cxt = CryptContext(schemes=['bcrypt'], deprecated='auto')

    def bcrypt(self, password: str) -> str:
        return self.pwd_cxt.hash(secret=password)

    def verify(self, plain_pass: str, hashed_pass: str) -> bool:
        return self.pwd_cxt.verify(secret=plain_pass, hash=hashed_pass)
