from fastapi import APIRouter, Depends, status

from sqlalchemy.orm import Session

import db
import models
import repository

__all__ = [
    'user_router'
]

user_router = APIRouter(
    prefix='/user',
    tags=['user']
)
router = user_router


@router.post('',
             response_model=models.UserOutModel,
             status_code=status.HTTP_201_CREATED)
def create_user(request: models.UserInput,
                session: Session = Depends(dependency=db.get_session)):
    return repository.create_user(session=session, request=request)


@router.get('/{id_}',
            response_model=models.UserOutModel,
            status_code=status.HTTP_200_OK)
def get_user(id_: int,
             session: Session = Depends(dependency=db.get_session)):
    return repository.get_user_from_id(session=session, id_=id_)
