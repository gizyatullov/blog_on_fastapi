from fastapi import APIRouter, Depends, status

from sqlalchemy.orm import Session

import db
import models
import repository
import utils

__all__ = [
    'blog_router'
]

blog_router = APIRouter(
    prefix='/blog',
    tags=['blog']
)
router = blog_router


@router.get('',
            response_model=list[models.ShowBlogModel],
            status_code=status.HTTP_200_OK)
def get_blogs(session: Session = Depends(dependency=db.get_session),
              current_user: models.UserInput = Depends(utils.get_current_user)):
    return repository.get_all_blog(session=session)


@router.get('/{id_}',
            response_model=models.BlogOutModel,
            status_code=status.HTTP_200_OK)
def get_blog(id_: int,
             session: Session = Depends(dependency=db.get_session),
             current_user: models.UserInput = Depends(utils.get_current_user)):
    return repository.get_blog_from_id(session=session, id_=id_)


@router.post('',
             response_model=models.BlogOutModel,
             status_code=status.HTTP_201_CREATED)
def create(request: models.BlogModel,
           session: Session = Depends(dependency=db.get_session),
           current_user: models.UserInput = Depends(utils.get_current_user)):
    return repository.create_new_blog(session=session, request=request)


@router.delete('/{id_}',
               status_code=status.HTTP_204_NO_CONTENT)
def blog_destroy(id_: int,
                 session: Session = Depends(dependency=db.get_session),
                 current_user: models.UserInput = Depends(utils.get_current_user)):
    return repository.del_blog(session=session, id_=id_)


@router.put('/{id_}',
            status_code=status.HTTP_202_ACCEPTED)
def blog_replacement(id_: int,
                     request: models.BlogModel,
                     session: Session = Depends(dependency=db.get_session),
                     current_user: models.UserInput = Depends(utils.get_current_user)):
    return repository.full_update_blog(session=session, id_=id_, request=request)
