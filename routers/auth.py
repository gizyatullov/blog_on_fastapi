from fastapi import APIRouter, status, Depends
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

import models
import db
import repository

__all__ = [
    'auth_router'
]

auth_router = APIRouter(
    tags=['authentication']
)
router = auth_router


@router.post('/login',
             status_code=status.HTTP_202_ACCEPTED)
def login(request: OAuth2PasswordRequestForm = Depends(),
          session: Session = Depends(dependency=db.get_session)):
    return repository.auth_user(session=session, request=request)
