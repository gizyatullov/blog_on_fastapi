from pathlib import Path

from dotenv import load_dotenv

BASE_DIR = Path(__file__).resolve().parent.parent

load_dotenv(
    dotenv_path=Path(BASE_DIR, '.env')
)
