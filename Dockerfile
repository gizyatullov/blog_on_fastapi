FROM python:3.10.6
RUN pip3 install --upgrade pip
WORKDIR /app
COPY . .
RUN pip3 install -r requirements.txt
EXPOSE 5194
# CMD ["uvicorn", "main:app", "--port", "5194"]