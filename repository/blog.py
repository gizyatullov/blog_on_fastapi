from fastapi import HTTPException, status

from sqlalchemy.orm import Session

from . import user

import db
import models

__all__ = [
    'get_all_blog',
    'get_blog_from_id',
    'create_new_blog',
    'del_blog',
    'full_update_blog',
]


def get_all_blog(session: Session):
    all_blogs = session.query(db.BlogMap).all()
    return all_blogs


def get_blog_from_id(session: Session, id_: int) -> db.BlogMap:
    blog = session.query(db.BlogMap).filter(db.BlogMap.id == id_)
    if not blog.scalar():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Blog with the id {id_} is not available.'
        )

    return blog.first()


def create_new_blog(session: Session, request: models.BlogModel) -> db.BlogMap:
    if not user.user_exist_from_id(session=session, id_=request.user_id):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'User with the id {request.user_id} is not available.'
        )

    new_blog = db.BlogMap(**request.dict())
    session.add(new_blog)
    session.commit()
    session.refresh(instance=new_blog)
    return new_blog


def del_blog(session: Session, id_: int) -> None:
    blog = session.query(db.BlogMap).filter(db.BlogMap.id == id_)

    if not blog.scalar():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Blog with the id {id_} is not available.'
        )

    blog.delete()
    session.commit()


def full_update_blog(session: Session, id_: int, request: models.BlogModel):
    blog = session.query(db.BlogMap).filter(db.BlogMap.id == id_)

    if not blog.scalar():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Blog with the id {id_} is not available.'
        )

    blog.update(request.dict())
    session.commit()

    return blog.first()
