from fastapi import HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

import db
import models
import utils

__all__ = [
    'user_exist_from_id',
    'create_user',
    'get_user_from_id',
    'auth_user',
]


def user_exist_from_id(session: Session, id_: int) -> bool:
    response = session.query(db.UserMap).filter(db.UserMap.id == id_).scalar()
    return response


def create_user(session: Session, request: models.UserInput) -> db.UserMap:
    request.password = utils.hash_.bcrypt(password=request.password)

    new_user = db.UserMap(**request.dict())
    session.add(new_user)
    session.commit()
    session.refresh(instance=new_user)
    return new_user


def get_user_from_id(session: Session, id_: int) -> db.UserMap:
    user = session.query(db.UserMap).filter(db.UserMap.id == id_)

    if not user.scalar():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'User with the id {id_} is not available.'
        )

    return user.first()


def auth_user(session: Session, request: OAuth2PasswordRequestForm):
    user = session.query(db.UserMap).filter(db.UserMap.email == request.username)

    if not user.scalar():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Invalid credentials.'
        )

    user = user.first()

    if not utils.hash_.verify(plain_pass=request.password, hashed_pass=user.password):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f'Invalid credentials.'
        )

    access_token = utils.create_access_token(data={'sub': user.email})
    return {'access_token': access_token, 'token_type': 'bearer'}
