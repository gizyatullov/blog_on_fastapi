from fastapi import FastAPI, Response, Depends, status, HTTPException
import uvicorn

from sqlalchemy.orm import Session

# инициализация настроек
import settings
# инициализация БД
import db
import routers

app = FastAPI(
    title='Learning FastAPI',
    description='I\'m learning FastAPI.',
    version='0.0.1',
    contact={
        'url': 'https://gitlab.com/gizyatullov',
    },
    license_info={
        'name': 'MIT',
    }
)


@app.get('/', tags=['hello, world!'])
def main():
    return [{'message': 'hello, world!'}]


app.include_router(router=routers.user_router)
app.include_router(router=routers.blog_router)
app.include_router(router=routers.auth_router)

# if __name__ == '__main__':
#     uvicorn.run(app='main:app', reload=True, port=5194)
