from sqlalchemy import Column, Integer, String, Text, ForeignKey
from sqlalchemy.orm import relationship

from . import Base


class BlogMap(Base):
    __tablename__ = 'blog'

    id = Column(Integer, primary_key=True)
    title = Column(String(50))
    body = Column(Text)
    user_id = Column(Integer, ForeignKey(column='user.id'))

    creator = relationship('UserMap', back_populates='blogs')
