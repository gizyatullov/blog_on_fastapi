from os import getenv

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import database_exists, create_database

URL = f"postgresql+psycopg2://{getenv('PG_USER')}:{getenv('PG_PASSWORD')}@{getenv('PG_IP')}:" \
      f"{getenv('PG_PORT')}/{getenv('PG_DB')}"

engine = create_engine(
    url=URL,
    echo=False
)

if not database_exists(url=engine.url):
    create_database(url=engine.url)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_session():
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()
