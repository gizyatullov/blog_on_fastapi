from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from . import Base


class UserMap(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String(50), index=True)
    email = Column(String(50), index=True)
    password = Column(String(100))

    blogs = relationship('BlogMap', back_populates='creator')
